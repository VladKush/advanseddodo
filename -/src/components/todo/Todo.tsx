import React, { useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import CreateIcon from "@mui/icons-material/Create";
import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import ClearIcon from "@mui/icons-material/Clear";
import "./Todo.css";

interface Todo {
  title: string;
  completed: boolean;
  subtasks: Subtask[];
}

interface Subtask {
  title: string;
  completed: boolean;
}

const TodoComponent: React.FC = () => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [inputValue, setInputValue] = useState("");
  const [editingTodoIndex, setEditingTodoIndex] = useState<number | null>(null);
  const [editingSubtaskIndex, setEditingSubtaskIndex] = useState<{
    todoIndex: number;
    subtaskIndex: number;
  } | null>(null);
  const [showAddSubtaskModal, setShowAddSubtaskModal] = useState(false);
  const [newSubtaskValue, setNewSubtaskValue] = useState("");
  const [currentTodoIndex, setCurrentTodoIndex] = useState<number | null>(null);

  const [position, setPosition] = useState({ x: 300, y: 200 });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const handleAddTodo = () => {
    if (inputValue.trim() !== "") {
      setTodos([
        ...todos,
        { title: inputValue, completed: false, subtasks: [] },
      ]);
      setInputValue("");
    }
  };

  const handleAddSubtask = (index: number) => {
    setCurrentTodoIndex(index);
    setShowAddSubtaskModal(true);
  };

  const handleSaveSubtask = () => {
    if (newSubtaskValue.trim() !== "") {
      const updatedTodos = [...todos];
      updatedTodos[currentTodoIndex!].subtasks.push({
        title: newSubtaskValue,
        completed: false,
      });
      setTodos(updatedTodos);
      setNewSubtaskValue("");
      setShowAddSubtaskModal(false);
    }
  };

  const handleCancelAddSubtask = () => {
    setShowAddSubtaskModal(false);
    setNewSubtaskValue("");
  };

  const handleTodoChange = (
    index: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const updatedTodos = [...todos];
    updatedTodos[index].title = e.target.value;
    setTodos(updatedTodos);
  };

  const handleSubtaskChange = (
    todoIndex: number,
    subtaskIndex: number,
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const updatedTodos = [...todos];
    updatedTodos[todoIndex].subtasks[subtaskIndex].title = e.target.value;
    setTodos(updatedTodos);
  };

  const handleDeleteTodo = (index: number) => {
    const updatedTodos = [...todos];
    updatedTodos.splice(index, 1);
    setTodos(updatedTodos);
  };

  const handleDeleteSubtask = (todoIndex: number, subtaskIndex: number) => {
    const updatedTodos = [...todos];
    updatedTodos[todoIndex].subtasks.splice(subtaskIndex, 1);
    setTodos(updatedTodos);
  };

  const handleEditTodo = (index: number) => {
    setEditingTodoIndex(index);
    setEditingSubtaskIndex(null);
  };

  const handleEditSubtask = (todoIndex: number, subtaskIndex: number) => {
    setEditingTodoIndex(null);
    setEditingSubtaskIndex({ todoIndex, subtaskIndex });
  };

  const handleSaveTodo = () => {
    setEditingTodoIndex(null);
  };

  const handleSaveSubtaskEdit = () => {
    setEditingSubtaskIndex(null);
  };

  const handleToggleTodo = (index: number) => {
    const updatedTodos = [...todos];
    updatedTodos[index].completed = !updatedTodos[index].completed;
    setTodos(updatedTodos);
  };

  const handleToggleSubtask = (todoIndex: number, subtaskIndex: number) => {
    const updatedTodos = [...todos];
    updatedTodos[todoIndex].subtasks[subtaskIndex].completed =
      !updatedTodos[todoIndex].subtasks[subtaskIndex].completed;
    setTodos(updatedTodos);
  };

  const handleMouseDown = (event: React.MouseEvent<HTMLDivElement>) => {
    const startX = event.clientX;
    const startY = event.clientY;
    const initialX = position.x;
    const initialY = position.y;

    const handleMouseMove = (event: MouseEvent) => {
      const deltaX = event.clientX - startX;
      const deltaY = event.clientY - startY;

      setPosition({
        x: initialX + deltaX,
        y: initialY + deltaY,
      });
    };

    document.addEventListener("mousemove", handleMouseMove);
    const handleMouseUp = () => {
      document.removeEventListener("mousemove", handleMouseMove);
    };
    document.addEventListener("mouseup", handleMouseUp);
  };

  return (
    <div
      onMouseDown={handleMouseDown}
      style={{
        padding: "50px",
        position: "absolute",
        left: position.x,
        top: position.y,
        cursor: "grab",
      }}
    >
      <div className="title">
        <input
          className="input"
          placeholder="Add task"
          type="text"
          value={inputValue}
          onChange={handleInputChange}
        />
        <button className="add-subtask" onClick={handleAddTodo}>
          <AddIcon />
        </button>
      </div>

      <ul className="task-wrapper">
        {todos.map((todo, index) => (
          <li className="task" key={index}>
            <div className="task-buttons-wrapper">
              {editingTodoIndex === index ? (
                <>
                  <input
                    className="input"
                    type="text"
                    value={todo.title}
                    onChange={(e) => handleTodoChange(index, e)}
                  />
                  <button
                    style={{ backgroundColor: "#5eab5e" }}
                    onClick={handleSaveTodo}
                  >
                    <CheckIcon />
                  </button>
                </>
              ) : (
                <>
                  <span className="title-task">{todo.title}</span>
                  <button onClick={() => handleEditTodo(index)}>
                    <CreateIcon />
                  </button>
                  <button
                    className="is-completed-wrapper"
                    onClick={() => handleToggleTodo(index)}
                  >
                    {todo.completed ? (
                      <span className="not-completed">
                        Mark Incomplete <CloseIcon />
                      </span>
                    ) : (
                      <span className="completed">
                        Mark complete
                        <CheckIcon />
                      </span>
                    )}
                  </button>
                </>
              )}
              {!showAddSubtaskModal && (
                <button
                  className="add-subtask"
                  onClick={() => handleAddSubtask(index)}
                >
                  <AddIcon />
                </button>
              )}
              <button
                className="delete-button"
                onClick={() => handleDeleteTodo(index)}
              >
                <ClearIcon />
              </button>
            </div>
            {todo.subtasks.length > 0 && (
              <ul className="sub-task-wrapper">
                {todo.subtasks.map((subtask, subtaskIndex) => (
                  <li className="task-buttons-wrapper" key={subtaskIndex}>
                    {editingSubtaskIndex &&
                    editingSubtaskIndex.todoIndex === index &&
                    editingSubtaskIndex.subtaskIndex === subtaskIndex ? (
                      <>
                        <input
                          className="input"
                          type="text"
                          value={subtask.title}
                          onChange={(e) =>
                            handleSubtaskChange(index, subtaskIndex, e)
                          }
                        />
                        <button
                          style={{ backgroundColor: "#5eab5e" }}
                          onClick={handleSaveSubtaskEdit}
                        >
                          <CheckIcon />
                        </button>
                      </>
                    ) : (
                      <>
                        <span className="subtask-title">{subtask.title}</span>
                        <button
                          onClick={() => handleEditSubtask(index, subtaskIndex)}
                        >
                          <CreateIcon />
                        </button>
                        <button
                          className="is-completed-wrapper"
                          onClick={() =>
                            handleToggleSubtask(index, subtaskIndex)
                          }
                        >
                          {subtask.completed ? (
                            <span className="not-completed">
                              Mark incomplete <CloseIcon />
                            </span>
                          ) : (
                            <span className="completed">
                              Mark complete
                              <CheckIcon />
                            </span>
                          )}
                        </button>
                      </>
                    )}
                    <button
                      className="delete-button"
                      onClick={() => handleDeleteSubtask(index, subtaskIndex)}
                    >
                      <ClearIcon />
                    </button>
                  </li>
                ))}
              </ul>
            )}
            {showAddSubtaskModal && currentTodoIndex === index && (
              <div className="add-sub-task-wrapper">
                <input
                  className="input"
                  type="text"
                  value={newSubtaskValue}
                  onChange={(e) => setNewSubtaskValue(e.target.value)}
                />
                <button
                  style={{ backgroundColor: "#5eab5e" }}
                  className="add-subtask"
                  onClick={handleSaveSubtask}
                >
                  <CheckIcon />
                </button>
                <button
                  className="delete-button"
                  onClick={handleCancelAddSubtask}
                >
                  <CloseIcon />
                </button>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TodoComponent;
